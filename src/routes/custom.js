const OrderPage = () => import(/* webpackChunkName: "order-page" */ '../views/OrderPage');
const HomePage = () => import(/* webpackChunkName: "home-page" */ '../views/HomePage.vue');
const MyPurchasesPage = () => import(/* webpackChunkName: "my-purchases" */ '../views/MyPurchasesPage.vue');
const UserSettingsPage = () => import(/* webpackChunkName: "my-settings" */ '../views/UserSettingsPage.vue');
const ConfirmOrderPage = () => import(/* webpackChunkName: "confirm-order-page" */ '../views/ConfirmOrderPage.vue');

const custom = [
  {
    path: '/',
    name: 'homePage',
    component: HomePage,
  },
  {
    path: '/my-purchases',
    name: 'myPurchasesPage',
    component: MyPurchasesPage,
  },
  {
    path: '/my-settings',
    name: 'userSettingsPage',
    component: UserSettingsPage,
  },
  {
    path: '/buy-crypt/:symbol',
    name: 'orderPage',
    component: OrderPage,
    props: true,
  },
  {
    path: '/confirm-order',
    name: 'confirmOrderPage',
    component: ConfirmOrderPage,
  },
];

export default custom;
