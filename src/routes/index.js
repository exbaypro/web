import Vue from 'vue';
import VueRouter from 'vue-router';

import mainMenu from './mainMenu.js';
import custom from './custom.js';
import store from '../store';

const Error404Page = () => import(/* webpackChunkName: "404" */ '../views/error404Page.vue');

const routes = [].concat(
  mainMenu,
  custom,
);

// don't put anything after 404
routes.push({
  path: '/*',
  name: 'error404Page',
  component: Error404Page,
});

Vue.use(VueRouter);

// Configure router
const router = new VueRouter({
  routes,
  linkActiveClass: 'active',
  mode: 'history',
});

router.beforeEach(function(to, from, next){
  store.commit('setLoading', false);
  next();
});

export default router;
