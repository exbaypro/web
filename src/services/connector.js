import axios from 'axios';
import store from '../store';

const baseDomain = process.env.API_HOST;
const baseURL = baseDomain + process.env.API_PREFIX;

const lang = process.env.LANG || 'en';

const instance = axios.create({
  baseURL,
  timeout: 10000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Accept-Language': lang,
  },
});

const token = store.state.customers.token;
if(token.length) {
  instance.defaults.headers['Authorization'] = 'Bearer ' + token;
}
instance.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';

instance.interceptors.response.use((response) => {
  console.warn('TODO: SUCCESS globaly handle xhr responses', response);
  return response;
}, (error) => {
  console.warn('TODO: ERROR globaly handle xhr responses', error);
  return Promise.reject(error);
});

export default instance;
