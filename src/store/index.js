import Vuex from 'vuex';
import Vue from 'vue';
import transactions from './modules/transactions';
import currencies from './modules/currencies'
import customers from './modules/customers';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    transactions,
    currencies,
    customers,
  },
  mutations: {
    setLoading(state, loading) {
      state.loading = loading;
    },
  },
  state: {
    loading: false,
  }
});

export default store;
