import CurrenciesRepository from './currenciesRepository';
import CustomersersRepository from './customersRepository';
import TransactionsRepository from './transactionsRepository';

const repositories = {
  currencies: CurrenciesRepository,
  transactions: TransactionsRepository,
  customers: CustomersersRepository,
};

export default {
  get(name) {
    return repositories[name];
  },
};
