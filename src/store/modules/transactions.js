import RepositoryFactory from '../../repositories/RepositoryFactory';

const transactionsRepository = RepositoryFactory.get('transactions');

export default {
  namespaced: true,
  state: {
    transactions: [],
    lastTransaction: {},
  },
  mutations: {
    addTransaction(state, transaction) {
      state.transactions.push(transaction);
    },
    setLastTransaction(state, transaction) {
      state.lastTransaction = transaction;
    },
    setTransations(state, transactions) {
      state.transactions = transactions;
    },
  },
  actions: {
    addTransaction({ commit }, transaction) {
      return transactionsRepository.createTransaction(transaction).then(result => {
        commit('addTransaction', transaction);
        commit('setLastTransaction', transaction);
        return result;
      });
    },
    getTransactions({ commit }) {
      return transactionsRepository.getTransactions().then(response => {
        const { status, data } = response;
        if (status === 200) {
          commit('setTransations', data.message);
        }

      });
    },
  },
  getters: {
    isAnyTransaction(state) {
      return state.transactions.length > 0;
    },
  },
};
