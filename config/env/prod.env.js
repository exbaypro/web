module.exports = {
  NODE_ENV: 'production',

  // front
  API_HOST: 'https://app-exbay.herokuapp.com',
  API_PREFIX: '/api/v1.0',
  LOCALE: 'ru',
  FALLBACK_LOCALE: 'ru',
};
