import Vue from 'vue';

const resource = '/transaction';

export default {
  // /transaction  POST
  createTransaction(transaction) {
    const { fromCurrency, toCurrency, toWallet, amount, customer } = transaction;

    return Vue.axios.post(resource, {
      fromCurrency,
      toCurrency,
      toWallet,
      amount,
      customer
    });
  },

  // /transaction  GET
  getTransactions() {
    return Vue.axios.get(resource);
  },
};
