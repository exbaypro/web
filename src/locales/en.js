export default {
  // custom
  'buy': 'Buy',
  'from': 'From',
  'to': 'To',
  'receive': 'Receive',
  'pay': 'Pay',
  'order': 'Order',
  'email': 'Email',
  'phone': 'Phone',
  'wallet': 'Wallet',
  'wallet-number': 'Wallet number',
  'required': 'Required',
  'checkout': 'Checkout',
  'settings': 'Settings',

  'search': 'Search...',

  //Login Form
  'login': 'Login',
  'password': 'Password',
  'password-re': 'Confirm Password',
  'sign-in': 'Sign In',
  'sign-up': 'Sign Up',
  'sign-off': 'Sign Off',

  //currencies
  'hrn.short': 'UAH',
  'hrn.full': '₴',
  'dollar.short': 'USD',
  'dollar.full': '$',

  // LastTransactions.vue
  'last-orders.last-purchases': 'Last Purchases',
  'last-orders.transaction-string': '{0} {1} in the amount of {2} {3}',

  // OrderPage.vue
  'order-page.buy-title': 'Buy {0} for the {1}',
  'order-page.total-for-payment': 'Total for payment',
  'order-page.max-crypt-limit-reached': 'Max amount for purchase is {0}',
  'order-page.min-crypt-limit-reached': 'Min amount for purchase is {0}',
    //ordersDetailForm.vue
    'order-page.order-detail-form.have-an-account': 'Already have an account?',
    'order-page.order-detail-form.create-an-account': 'Create an account?',

  //Main Page
  'main.currency-list.write-us': 'Write Us',
  'main.currency-list.did-not-find': 'Did not find what you were looking for?',

  // Menu
  'menu.home': 'Home',
  'menu.about': 'About',
  'menu.faq': 'FAQ',
  'menu.reviews': 'Reviews',
  'menu.airdrops': 'Air Drops',
  'menu.where-to-store': 'Where to store the crypt?',
  'menu.sales': 'Discounts',
  'menu.contacts': 'Contacts',
  'menu.user.purchases': 'Purchases',
  'menu.user.settings': 'Settings',

  'pages.confirm-page.title': 'Confirm Order',
};
