import RepositoryFactory from '../../repositories/RepositoryFactory';

const currenciesRepository = RepositoryFactory.get('currencies');

export default {
  namespaced: true,
  state: {
    currentCurrency: {},
    currencyList: [],
  },
  mutations: {
    setCurrencyList(state, currencyList) {
      state.currencyList = currencyList;
    },
    setCurrentCurrency(state, currentCurrency) {
      state.currentCurrency = currentCurrency;
    },
  },
  actions: {
    fetchCurrencyList({ commit }) {
      commit('setLoading', true, { root: true });

      currenciesRepository.getList().then((response) => {
        const data = response.data;
        if (response.status === 200) {
          commit('setCurrencyList', data.message);
          commit('setLoading', false, { root: true });
        }
      });
    },
    fetchCurrentCurrency({ commit }, symbol) {
      commit('setLoading', true, { root: true });

      currenciesRepository.getCurrency(symbol).then((response) => {
        const data = response.data;
        if (response.status === 200) {
          commit('setCurrentCurrency', data.message);
          commit('setLoading', false, { root: true });
        }
      });
    },
    refreshCurrencyList({ commit }) {
      setTimeout(() => {
        commit('setCurrencyList', []);
      }, 1500);
    },
  },
};
