import Vue from 'vue';
import RepositoryFactory from "../../repositories/RepositoryFactory";

const customersRepository = RepositoryFactory.get("customers");

const loginAction = (commit, dispatch, token, expires) => {
  localStorage.setItem("user-token", token);
  localStorage.setItem("user-token-expires", expires);
  commit("setToken", token);
  commit("setExpires", expires);
  commit("setLogged", true);

  Vue.axios.defaults.headers['Authorization'] = 'Bearer ' + token;

  dispatch('fetchUser');
};

const logoutAction = (commit) => {
  localStorage.setItem("user-token", '');
  localStorage.setItem("user-token-expires", 0);
  commit("setToken", '');
  commit("setExpires", 0);
  commit("setLogged", false);

  Vue.axios.defaults.headers['Authorization'] = '';
};

export default {
  namespaced: true,
  state: {
    customer:  {},
    token: localStorage.getItem("user-token") || "",
    expires: localStorage.getItem('user-token-expires') || 0,
    status: "",
    logged: false,
    orders: [],
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    setExpires(state, expires) {
      state.expires = expires;
    },
    setLogged(state, isLogged) {
      state.logged = isLogged;
    },
    setCustomerData(state, customer) {
      state.customer = customer;
    },
    setCustomerOrders(state, orders) {
      state.orders = orders;
    },
  },
  actions: {
    logout({ commit }) {
      logoutAction(commit);
    },
    fetchUser({ commit }) {
      return customersRepository.get().then(response => {
        const { status, data } = response;
        if (status === 200) {
          commit('setCustomerData', data.message);
        }
      });
    },
    registration({ commit, dispatch }, data) {
      return customersRepository.registration(data).then(response => {
        const { status, data } = response;
        if (status === 201) {
          const { token, expires } = data.message;
          loginAction(commit, dispatch, token, expires);
        }
      });
    },
    authorization({ commit, dispatch }, data) {
      return customersRepository.authorization(data).then(response => {
        const { status, data } = response;
        if (status === 200) {
          const { token, expires } = data.message;
          loginAction(commit, dispatch, token, expires);
        }
      });
    },
    getOrders({ commit }) {
      return customersRepository.getOrders().then(response => {
        const { status, data } = response;
        if (status === 200) {
          commit('setCustomerOrders', data.message.orders);
        }
      });
    },
  },
  getters: {
    isAuthenticated: state => !!state.token
  }
};
