export default {
  // custom
  'buy': 'Купить',
  'from': 'От',
  'to': 'До',
  'receive': 'Получаете',
  'pay': 'Платите',
  'order': 'Заказ',
  'email': 'Email',
  'phone': 'Телефон',
  'wallet': 'Кошелек',
  'wallet-number': 'Номер кошелька',
  'required': 'Обязательно',
  'checkout': 'Отправить',
  'settings': 'Настройки',

  'search': 'Поиск...',

  //Login Form
  'login': 'Логин',
  'password': 'Пароль',
  'password-re': 'Подтверждение пароля',
  'sign-in': 'Вход',
  'sign-up': 'Регистрация',
  'sign-off': 'Выход',

  //currencies
  'hrn.short': 'UAH',
  'hrn.full': '₴',
  'dollar.short': 'USD',
  'dollar.full': '$',

  // LastTransactions.vue
  'last-orders.last-purchases': 'Последние покупки',
  'last-orders.transaction-string': '{0} {1} в количестве {2} {3}',

  // OrderPage.vue
  'order-page.buy-title': 'Покупаете {0} за {1}',
  'order-page.total-for-payment': 'Всего к оплате',
  'order-page.max-crypt-limit-reached': 'Максимальное количество для покупки {0}',
  'order-page.min-crypt-limit-reached': 'Минимальное количество для покупки {0}',
  //ordersDetailForm.vue
  'order-page.order-detail-form.have-an-account': 'Уже зарегестрированы?',
  'order-page.order-detail-form.create-an-account': 'Создать аккаунт?',

  //Main Page
  'main.currency-list.write-us': 'Напишите нам',
  'main.currency-list.did-not-find': 'Не нашли то что искали?',

  // Menu
  'menu.home': 'Домой',
  'menu.about': 'О нас',
  'menu.faq': 'FAQ',
  'menu.reviews': 'Отзывы',
  'menu.airdrops': 'Air Drops',
  'menu.where-to-store': 'Где хранить криптовалюту?',
  'menu.sales': 'Скидки',
  'menu.contacts': 'Контакты',
  'menu.user.purchases': 'Покупки',
  'menu.user.settings': 'Настройки',

  'pages.confirm-page.title': 'Подтверждение Заказа',
};
