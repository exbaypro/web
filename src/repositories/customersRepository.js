import Vue from 'vue';

const resource = '/customer';

export default {
  // /customer POST
  registration(data) {
    return Vue.axios.post(resource, data);
  },

  // /customer GET
  get() {
    return Vue.axios.get(resource);
  },

  // /customer PUT
  authorization(data) {
    return Vue.axios.put(resource, data);
  },

  // /customer/order GET
  getOrders(data) {
    return Vue.axios.get(`${resource}/order`);
  },
};
