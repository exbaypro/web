const AboutPage = () => import(/* webpackChunkName: "about-page" */ '../views/AboutPage.vue')
const FAQPage = () => import(/* webpackChunkName: "faq-page" */ '../views/FAQPage.vue')
const ReviewsPage = () => import(/* webpackChunkName: "reviews-page" */ '../views/ReviewsPage.vue')
const AirDropsPage = () => import(/* webpackChunkName: "airDrops-page" */ '../views/AirDropsPage.vue')
const WhereStorePage = () => import(/* webpackChunkName: "whereStore-page" */ '../views/WhereStorePage.vue')
const SalesPage = () => import(/* webpackChunkName: "sales-page" */ '../views/SalesPage.vue')
const ContactsPage = () => import(/* webpackChunkName: "contacts-page" */ '../views/ContactsPage.vue')

const mainMenu = [
  {
    path: '/about',
    name: 'aboutPage',
    component: AboutPage,
    title: 'menu.about'
  },
  {
    path: '/faq',
    name: 'faqPage',
    component: FAQPage,
    title: 'menu.faq'
  },
  {
    path: '/reviews',
    name: 'reviewsPage',
    component: ReviewsPage,
    title: 'menu.reviews'
  },
  {
    path: '/airdrops',
    name: 'airDropsPage',
    component: AirDropsPage,
    title: 'menu.airdrops'
  },
  {
    path: '/where',
    name: 'whereStorePage',
    component: WhereStorePage,
    title: 'menu.where-to-store'
  },
  {
    path: '/sales',
    name: 'salesPage',
    component: SalesPage,
    title: 'menu.sales'
  },
  {
    path: '/contacts',
    name: 'contactsPage',
    component: ContactsPage,
    title: 'menu.contacts'
  }
]

export default mainMenu
