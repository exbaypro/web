if(!String.prototype.cap) {
  String.prototype.cap = function() {
    return this.slice(0,1).toUpperCase() + this.slice(1).toLowerCase();
  };
}
