// core-js
import 'core-js/features/promise';
import 'core-js/es/object/assign';

import './services/extensions';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';

import './styles/css/pre-icons.css';
import 'cryptocoins-icons/webfont/cryptocoins.css';

import './styles/css/main.css';

import Vue from 'vue';


// Import Vue App, routes, store, i18n
import Fragment from 'vue-fragment';
import VueMeta from 'vue-meta'
import App from './App';
import store from './store';
import router from './routes';
import i18n from './i18n';
import VueTimeago from 'vue-timeago';
import connector from '@/services/connector';
import VueAxios from 'vue-axios';

const lang = process.env.LANG || 'en';

Vue.use(Fragment.Plugin);

Vue.use(VueAxios, connector);

Vue.use(VueTimeago, {
  name: 'Timeago',
  locale: lang, // Default locale
  locales: {
    ru: require('date-fns/locale/ru'),
    en: require('date-fns/locale/en'),
  },
});

Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
});

export default new Vue({
  el: '#app',
  store,
  i18n,
  render: h => h(App),
  router,
});
