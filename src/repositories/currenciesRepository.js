import Vue from 'vue';

const resource = '/currency';

export default {
  // /currency
  getList() {
    return Vue.axios.get(`${resource}`);
  },

  // /currency/:symbol
  getCurrency(currencySymbol) {
    return Vue.axios.get(`${resource}/${currencySymbol}`);
  },
};
